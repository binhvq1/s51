package com.devcamp.javabasic_s30;

public class Customer {

    byte myByte;
    short myShortNumber;
    int myIntNumber;
    long myLongNumber;
    float myFloatNumber;
    double myDoubleNumber;
    boolean myBoolean;
    char myChar;

    public static void main(String[] args) {
        Customer customer = new Customer();
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myBoolean);
        System.out.println(customer.myChar);
        System.out.println("Run 1");
        byte myByte = 10;
        short myShort = 2001;
        customer =  new Customer(myByte, myShort, 5, 7001, 560.3f, 203.6,false, 'A');
    }

    public Customer() {
        myByte = 127;
        myShortNumber = 2022;
        myIntNumber = 5;
        myLongNumber = 2021;
        myFloatNumber = 9.99f;
        myDoubleNumber = 123.4;
        myBoolean = false;
        myChar = 'P';
    }

    public Customer(byte byteNum, short shortNum, int intNum, long longNum, float floatNum, 
    double doubleNum, boolean bool, char letter) {
        myByte = byteNum;
        myShortNumber = shortNum;
        myIntNumber = intNum;
        myLongNumber = longNum;
        myFloatNumber = floatNum;
        myDoubleNumber = doubleNum;
        myBoolean = bool;
        myChar = letter;
    }
}
