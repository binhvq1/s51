package com.devcamp.javabasic_s30;

public class Customer {

    static byte myByte;
    static short myShortNumber;
    int myIntNumber;
    long myLongNumber;
    float myFloatNumber;
    double myDoubleNumber;
    boolean myBoolean;
    char myChar;

    public static void main(String[] args) {
        Customer customer = new Customer();
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myBoolean);
        System.out.println(customer.myChar);
        System.out.println("Run 1");
        myByte = 10;
        myShortNumber = 2001;
        customer =  new Customer(myByte, myShortNumber, 5, 7001, 560.3f, 203.6,false, 'A');
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myBoolean);
        System.out.println(customer.myChar);
        System.out.println("Run 2");
        myByte = 20;
        myShortNumber = 2002;
        customer =  new Customer(myByte, myShortNumber, 5, 7001, 560.3f, 203.6,false, 'B');
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myBoolean);
        System.out.println(customer.myChar);
        System.out.println("Run 3");
        myByte = 30;
        myShortNumber = 2003;
        customer =  new Customer(myByte, myShortNumber, 5, 7001, 560.3f, 203.6,false, 'C');
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myBoolean);
        System.out.println(customer.myChar);
        System.out.println("Run 4");
        myByte = 40;
        myShortNumber = 2004;
        customer =  new Customer(myByte, myShortNumber, 5, 7001, 560.3f, 203.6,false, 'D');
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myBoolean);
        System.out.println(customer.myChar);
        System.out.println("Run 2");
        myByte = 50;
        myShortNumber = 2005;
        customer =  new Customer(myByte, myShortNumber, 5, 7001, 560.3f, 203.6,false, 'E');
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myBoolean);
        System.out.println(customer.myChar);
    }

    public Customer() {
        myByte = 127;
        myShortNumber = 2022;
        myIntNumber = 5;
        myLongNumber = 2021;
        myFloatNumber = 9.99f;
        myDoubleNumber = 123.4;
        myBoolean = false;
        myChar = 'P';
    }

    public Customer(byte byteNum, short shortNum, int intNum, long longNum, float floatNum, 
    double doubleNum, boolean bool, char letter) {
        myByte = byteNum;
        myShortNumber = shortNum;
        myIntNumber = intNum;
        myLongNumber = longNum;
        myFloatNumber = floatNum;
        myDoubleNumber = doubleNum;
        myBoolean = bool;
        myChar = letter;
    }
}
